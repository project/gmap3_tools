(function ($) {

/**
 * Create google map.
 *
 * @param object mapOptions
 *   Object of gmap options.
 */
function gmap3ToolsCreateMap(mapOptions) {
  // Create map.
  var map = new google.maps.Map(document.getElementById(mapOptions.mapId), {
    center: new google.maps.LatLng(mapOptions.centerX, mapOptions.centerY),
    zoom: mapOptions.zoom,
    mapTypeId: mapOptions.mapTypeId
  });

  // Create markers for this map.
  $.each(mapOptions.markers, function (i, markerData) {
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(markerData.lat, markerData.lng),
      map: map
    });
    if (typeof markerData.title != 'undefined') {
      marker.setTitle(markerData.title);
    }
    if (typeof markerData.content != 'undefined') {
      google.maps.event.addListener(marker, 'click', function(e) {
        var infobox = new google.maps.InfoWindow({
          position: marker.getPosition(),
          map: map,
          content: markerData.content
        });
      });
    }
  });

  if (mapOptions.centerMarkers) {
    gmap3ToolsCenterMarkers(map, mapOptions.markers);
  }

  return map;
}

/**
 * Center markers on map.
 */
function gmap3ToolsCenterMarkers(map, markersData) {
  var centerLat = 0;
  var centerLng = 0;
  $.each(markersData, function (i, markerData) {
    centerLat += markerData.lat;
    centerLng += markerData.lng;
  });
  centerLat /= markersData.length;
  centerLng /= markersData.length;
  map.setCenter(new google.maps.LatLng(centerLat, centerLng));
}

$(document).ready(function () {
  // Create all google maps.
  $.each(Drupal.settings.gmap3_tools.maps, function(i, map) {
    gmap3ToolsCreateMap(map);
  });
});

})(jQuery);
